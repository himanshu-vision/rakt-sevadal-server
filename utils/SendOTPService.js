var http = require("http");
var msg91config = require('../constants/msg91');

// function sendOTP(mobile, otp) {
//     var options = {
//         "method": "POST",
//         "hostname": "control.msg91.com",
//         "port": null,
//         "path": "/api/sendotp.php?template=&otp_length=" + msg91config.OTP_LENGTH + "&authkey=" + msg91config.AUTHKEY + "&message=OTP to verify mobile number on RaktSevadal is &sender=&mobile=" + mobile + "&otp=" + otp + "&otp_expiry=&email=",
//         "headers": {}
//     };

//     console.log('request');
//     var req = http.request(options, function (res) {
//         var chunks = [];
//         console.log('request 2');

//         res.on("data", function (chunk) {
//             chunks.push(chunk);
//         });

//         console.log('fff');
//         res.on("end", function () {
//             var body = Buffer.concat(chunks);
//             console.log(body.toString());
//         });
//     });

//     req.end();
// }

function sendOTP(mobile, otp) {
    const message = encodeURIComponent("OTP to verify mobile number on RaktSevadal is " + otp);
    var options = {
        "method": "GET",
        "hostname": "api.msg91.com",
        "port": null,
        "path": "/api/sendhttp.php?sender=" + msg91config.SENDR + "&route=4&mobiles=" + mobile + "&authkey=" + msg91config.AUTHKEY + "&country=91&message=" + message,
        "headers": {}
    };

    var req = http.request(options, function (res) {
        var chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            var body = Buffer.concat(chunks);
            console.log(body.toString());
        });
    });

    req.end();
}

module.exports = sendOTP;
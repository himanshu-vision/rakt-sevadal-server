var OTPObjectSchema = {
    mobile: String,
    created_at: { type: Date, default: Date.now },
    expire: Date,
    otp: String
};
module.exports = OTPObjectSchema;
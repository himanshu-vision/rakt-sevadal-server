var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');

var AuthRoute = require('../../utils/Auth');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

var User = require('./User');

// utils
var sendOTP = require('../../utils/SendOTPService');

var error = { success: false, response: 'something went wrong' };

router.get('/test', function(req, res) {
	res.json({ success: true, response: 'Server is up' });
});

router.post('/generateOTP', function(req, res) {
	const mobile = req.body.mobile;
	console.log('req', req);
	//find user with mobile is exsits
	User.find({ mobile: mobile }, function(err, users) {
		console.log('users', users);
		var OTP = GenerateOTPObject(mobile);
		if (Array.isArray(users) && users.length == 0) {
			//New User
			// create user object
			User.create(
				{
					mobile: mobile,
					otp: [OTP],
				},
				function(err, user) {
					if (err)
						return res.status(500).send({
							success: false,
							response: 'There was a problem adding the information to the database',
						});
					res.json({ success: true, response: 'OTP sent', otp: OTP.otp });
				}
			);
		} else {
			// returning user
			User.findOneAndUpdate({ mobile: mobile }, { $push: { otp: OTP } }, { new: true }, function(err, doc) {
				if (err)
					return res.status(500).send({
						success: false,
						response: 'There was a problem adding the information to the database',
					});
				res.json({ success: true, response: 'OTP sent', otp: OTP.otp });
			});
		}
		console.log('OTP', OTP);
		sendOTP(mobile, OTP.otp);
	});
});

router.post('/verifyOTP', function(req, res) {
	var mobile = req.body.mobile;
	var userOtp = req.body.otp;

	var token = jwt.sign({ mobile }, 'superSecret', {
		expiresIn: 1440 * 7 * 24, // expires in 24 hours
	});

	User.find({ mobile: mobile }, function(err, users) {
		if (err)
			return res
				.status(500)
				.send({ success: false, response: 'There was a problem adding the information to the database' });
		const otpArr = users[0].otp;
		const generatedOTPObject = otpArr[otpArr.length - 1];
		if (generatedOTPObject.otp == userOtp) {
			res.status(500).send({ success: true, response: 'Authenticated', token });
		}
		res
			.status(200)
			.send({ success: false, response: 'There was a problem adding the information to the database' });
	});
});

router.post('/update/basic', function(req, res) {
	AuthRoute(req, res, (req, res) => {
		const mobile = req.decoded.mobile;
		const fullName = req.body.full_name;
		const age = req.body.age;
		const alternateContactNumber = req.body.alt_contact;

		User.findOneAndUpdate(
			{ mobile: mobile },
			{ $set: { age: age, display_name: fullName, alternate_contact: alternateContactNumber } },
			{ new: true },
			function(err, doc) {
				if (err)
					return res.status(500).send({
						success: false,
						response: 'There was a problem adding the information to the database',
					});
				res.json({ success: true, response: 'Updated' });
			}
		);
	});
});

router.post('/update/gender', function(req, res) {
	AuthRoute(req, res, (req, res) => {
		const mobile = req.decoded.mobile;
		const gender = req.body.gender;

		User.findOneAndUpdate({ mobile: mobile }, { $set: { gender: gender } }, { new: true }, function(err, doc) {
			if (err)
				return res
					.status(500)
					.send({ success: false, response: 'There was a problem adding the information to the database' });
			res.json({ success: true, response: 'Updated' });
		});
	});
});

router.post('/update/blood_group', function(req, res) {
	AuthRoute(req, res, (req, res) => {
		const mobile = req.decoded.mobile;
		const bloodGroup = req.body.blood_group;

		User.findOneAndUpdate({ mobile: mobile }, { $set: { blood_group: bloodGroup } }, { new: true }, function(
			err,
			doc
		) {
			if (err)
				return res
					.status(500)
					.send({ success: false, response: 'There was a problem adding the information to the database' });
			res.json({ success: true, response: 'Updated' });
		});
	});
});

router.post('/update/location', function(req, res) {
	AuthRoute(req, res, (req, res) => {
		const mobile = req.decoded.mobile;
		const location = req.body.location;

		User.findOneAndUpdate({ mobile: mobile }, { $set: { location: location } }, { new: true }, function(err, doc) {
			if (err)
				return res
					.status(500)
					.send({ success: false, response: 'There was a problem adding the information to the database' });
			res.json({ success: true, response: 'Updated' });
		});
	});
});

router.get('/basic', function(req, res) {
	AuthRoute(req, res, (req, res) => {
		const mobile = req.decoded.mobile;

		User.findOne(
			{ mobile: mobile },
			'display_name mobile age blood_group gender alternate_contact location -_id',
			function(err, users) {
				if (err) res.json({ success: false, response: 'something went wrong' });
				res.json({ success: true, response: users });
			}
		);
	});
});

function GenerateOTPObject(mobile) {
	var now = new Date();
	var otp = {
		mobile: mobile,
		created_at: now,
		expire: new Date(now.getTime() + 15 * 60000),
		otp: Math.floor(Math.random() * 9000) + 1000,
	};
	return otp;
}

module.exports = router;

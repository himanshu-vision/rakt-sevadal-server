// User.js
var mongoose = require('mongoose');
var OTPObjectSchema = require('../otp/OTP');

var UserSchema = new mongoose.Schema({
    mobile: String,
    display_name: String,
    dob: Date,
    age: String,
    gender: String,
    blood_group: String,
    is_verified: Boolean,
    city: String,
    alternate_contact: String,
    email: String,
    role: { type: [String], default: ['user'] },
    otp: { type: [OTPObjectSchema] },
    location: Object
}, { collection: 'users' });
mongoose.model('User', UserSchema);
module.exports = mongoose.model('User');
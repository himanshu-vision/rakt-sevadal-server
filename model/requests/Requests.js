// User.js
var mongoose = require('mongoose');
var User = require('../user/User');

var Requests = new mongoose.Schema(
	{
		user_id: String,
		name: String,
		blood_group: String,
		units: Number,
		is_urgent: Boolean,
		contact_number: String,
		latitude: String,
		longitude: String,
		address: String,
		city: String,
		locality: String,
		district: String,
		state: String,
		require_upto: Date,
		locationObjct: Object,
		accepted: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
	},
	{ collection: 'requests' }
);
mongoose.model('Requests', Requests);
module.exports = mongoose.model('Requests');

var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');

var AuthRoute = require('../../utils/Auth');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

var Requests = require('./Requests');
var User = require('../user/User');

var error = { success: false, response: 'something went wrong' };

router.post('/add', function(req, res) {
	AuthRoute(req, res, (req, res) => {
		const mobile = req.decoded.mobile;
		const requestObject = req.body.payload;

		User.findOne({ mobile: mobile }, function(err, user) {
			if (err) res.json(error);
			requestObject.user_id = user._id;
			Requests.create(requestObject, function(err, requestData) {
				if (err) {
					return res.status(500).send({
						success: false,
						response: 'There was a problem adding the information to the database',
					});
				}
				res.json({ success: true, response: 'Success' });
			});
		});
	});
});

router.get('/fetch', function(req, res) {
	AuthRoute(req, res, (req, res) => {
		Requests.find({}, function(err, requestData) {
			if (err) {
				return res.status(500).send({
					success: false,
					response: 'There was a problem getting the information.',
				});
			}
			res.json({ success: true, response: requestData });
		});
	});
});

router.get('/fetchMine', function(req, res) {
	AuthRoute(req, res, (req, res) => {
		const mobile = req.decoded.mobile;
		User.findOne({ mobile: mobile }, function(err, user) {
			if (err)
				return res
					.status(500)
					.send({ success: false, response: 'There was a problem getting the information' });
			const userId = user._id;
			Requests.find({}, function(err, requestData) {
				console.log(err, requestData);
				if (err) {
					return res.status(500).send({
						success: false,
						response: 'There was a problem getting the information.',
					});
				}
				console.log(userId, 'tattti');
				const filterData = requestData.filter(item => item.user_id == userId);
				res.json({ success: true, response: filterData });
			});
		});
	});
});

router.get('/details/:id', function(req, res) {
	AuthRoute(req, res, (req, res) => {
		const id = req.params.id;

		Requests.findOne({ _id: id })
			.populate('accepted')
			.exec(function(err, doc) {
				if (err)
					return res
						.status(500)
						.send({ success: false, response: 'There was a problem getting the information' });
				res.json({ success: true, response: doc });
			});
	});
});

router.post('/accept/:id', function(req, res) {
	AuthRoute(req, res, (req, res) => {
		const id = req.params.id;
		const mobile = req.decoded.mobile;
		User.findOne({ mobile: mobile }, function(err, user) {
			if (err)
				return res
					.status(500)
					.send({ success: false, response: 'There was a problem getting the information' });
			Requests.findOneAndUpdate({ _id: id }, { $push: { accepted: user } }, { new: true }, function(err, doc) {
				if (err)
					return res.status(500).send({
						success: false,
						response: 'There was a problem adding the information to the database',
					});

				res.json({ success: true, response: doc });
			});
			// Requests.findOne({ _id: id }, function(err, data) {
			// 	return res
			// 		.status(500)
			// 		.send({ success: false, response: 'There was a problem getting the information' });
			// });
			// Requests.findOneAndUpdate({ _id: id }, { $push: { accepted: user } }, { new: true }, function(err, data) {
			// 	console.log(err);
			// 	if (err)
			// 		return res
			// 			.status(500)
			// 			.send({ success: false, response: 'There was a problem getting the information' });
			// 	console.log('fff', data);
			// 	res.json({ success: true, response: data });
			// });
		});
	});
});

module.exports = router;

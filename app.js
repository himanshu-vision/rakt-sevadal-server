var express = require('express');
var cors = require('cors');
var app = express();

app.use(cors());

var db = require('./db');

var UserController = require('./model/user/UserController');
var RequestController = require('./model/requests/RequestController');

app.use('/users', UserController);
app.use('/request', RequestController);

module.exports = app;
